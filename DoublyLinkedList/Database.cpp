#include "Database.h"
#include "Array.h"
#include <iostream>
using std::string;
using std::cout;
using std::cin;
using std::endl;
//First note to say that in early stages of this project I done alotof 
//work with michael campell so alot of our logic may seem very similar


//========================================================================
// Name: Append
// Desc: Simply calls the underlying lists append member function
// Args: p_player - The player to append
// Ret: none
// Complexity: 0(c)
//========================================================================
void Database::append(Player p_player)
{
	m_list.append(p_player);
}

//========================================================================
// Name: Insert
// Desc: Adds a player at a specified position in the Databases list
// Args: p_player - The player to append, p_position where to ass
// Ret: none
// Complexity: 0(n)
// Timing: 30 Players to start
// Insert to pos 1 : 0ms
// Insert at pos 99 : 0ms
// Insert at pos 10 : 0ms
// Insert at pos 29(second last) : 0ms
// Insert at 100000 with 3000000 Players : 14ms
//========================================================================
void Database::insert(int p_position, Player p_player)
{
	m_list.insert(p_position, p_player);
}

//========================================================================
// Name: Print
// Desc: Print that database to screen
// Args: none
// Ret: none
//========================================================================
void Database::print()
{
	DListIterator<Player> itr = m_list.getIterator();
	std::cout << "Player Database: ";
	for (itr.start(); itr.valid(); itr.forth())
	{
		itr.item().print();
	}
}

//========================================================================
// Name: Remove
// Desc: Remove a player at a specified position from the Databases' list
// Args: p_position
// Ret: none
// Complexity: 0(n)
// Timing: 30 Players
// Remove from pos 1: 0ms
// Remove from pos 30: 0ms
// Remove from pos 29: 0ms
// Remove from pos 100000 with 3000000 Players : 13ms
//========================================================================
void Database::remove(int p_position)
{
	m_list.remove(p_position);
}

//========================================================================
// Name: Search
// Desc: search for players with a specific value
// Args: p_key - search key (first, last, lvl, exp)
//       res - refernce to the reults list (reset each search)
// Ret: none
// Complexity: 0(n)
// Time: 0 ms with 30 players
// Note: Done some work with Michael here code may seem similar we 
// worked out logic together. Same for all searches
//========================================================================
void Database::search(int p_choice, int p_data, DLinkedList<Player>& p_results)
{
	enum keys{ LV, EXP };
	DListIterator<Player> itr = m_list.getIterator();
	if (p_choice == LV)
	{
		for (itr.start(); itr.valid(); itr.forth())
		{
			if (itr.item().getLvl() == p_data)
			{
				p_results.append(itr.item());
			}
		}
	}
	else if (p_choice == EXP)
	{
		for (itr.start(); itr.valid(); itr.forth())
		{
			if (itr.item().getExp() == p_data)
			{
				p_results.append(itr.item());
			}
		}
	}
}

//========================================================================
// Name: Search
// Desc: search for players with a specific value
// Args: p_key - search key (first, last)
//		 p_data - data item to search for
//       res - refernce to the reults list (reset each search)
// Ret: none
// Complexity: 0(n)
// Time: 0ms with 30 players
//========================================================================
void Database::search(int p_choice, string p_data, DLinkedList<Player>& p_results)
{
	enum keys {FIRST = 2, LAST = 3};
	DListIterator<Player> itr = m_list.getIterator();
	if (p_choice == FIRST)
	{
		for (itr.start(); itr.valid(); itr.forth())
		{
			if (itr.item().getFirst() == p_data)
			{
				p_results.append(itr.item());
			}
		}
	}
	else if (p_choice == LAST)
	{
		for (itr.start(); itr.valid(); itr.forth())
		{
			if (itr.item().getLast() == p_data)
			{
				p_results.append(itr.item());
			}
		}
	}
}

//========================================================================
// Name: Search
// Desc: search for players with a specific value
// Args: p_key - search key (first, last)
//		 p_min - min data of range to search
//		 p_max - max data of range to search
//       res - refernce to the reults list (reset each search)
// Ret: none
// Complexity: 0(n)
// Time: 0ms with 30 players
//========================================================================
void Database::search(int p_choice, int p_min, int p_max, DLinkedList<Player>& p_results)
{
	enum keys{LV = 1, EXP = 2};
	DListIterator<Player> itr = m_list.getIterator();
	if (p_choice == LV)
	{
		for (itr.start(); itr.valid(); itr.forth())
		{
			if (itr.item().getLvl() >= p_min && itr.item().getLvl() <= p_max)
			{
				p_results.append(itr.item());
			}
		}
	}
	else if (p_choice == EXP)
	{
		for (itr.start(); itr.valid(); itr.forth())
		{
			if (itr.item().getExp() >= p_min && itr.item().getExp() <= p_max)
			{
				p_results.append(itr.item());
			}
		}
	}
}

//========================================================================
// Name: GetAvgExperience
// Desc: get the average experience of players in a specific layer
// Args: p_lvl - the level of which to average the experience points
// Ret: avg the average experience at p_lvl
// Complexity: 0(n)
// Time: 0ms with 30 players
//========================================================================
int Database::getAvgExperience(int lvl)
{
	int sum = 0;
	int count = 0;
	DListIterator<Player> itr = m_list.getIterator();
	for (itr.start(); itr.valid(); itr.forth())
	{
		if(itr.item().getLvl() == lvl)
		{
			sum = sum + itr.item().getExp();
			count ++;
		}
	}
	if(count > 0)
	{
		return sum/count;
	}
	return 0;
}

//========================================================================
// Name: RemoveDuplicates
// Desc: remove all duplicate players (same name case sensitive) from the database
// Args: none
// Ret: number of dupes
// Complexity: 0(n^2)
//========================================================================
int Database::removeDuplicates()
{
	Database removeDupes;
	DLinkedList<int> dupePositions;//Stores to position of each duplicate to delete
	DListIterator<Player> itr = m_list.getIterator();

	int count = 0;
	for (itr.start(); itr.valid(); itr.forth())
	{
		if (removeDupes.contains(itr.item())) 
		{
			dupePositions.append(count);
		}
		else
		{
			removeDupes.append(itr.item());
		}
		count++;
	}

	DListIterator<int> itr2 = dupePositions.getIterator();
	for (itr2.start(); itr2.valid(); itr2.forth())
	{
		m_list.remove(itr2.item());
	}

	return dupePositions.size();
}
//========================================================================
// Name: Contains
// Desc: check if database contains a player
// Args: p_p - the player to check (also looks like a face)
// Ret: boolean - true if database contains p_p
// Complexity: 0(n)
//========================================================================
bool Database::contains(Player& p_p)
{
	DListIterator<Player> itr = m_list.getIterator();

	for (itr.start(); itr.valid(); itr.forth())
	{
		if(itr.item().equals(p_p))
		{
			return true;
		}
	}
	return false;
}

//========================================================================
// Name: getList
// Desc: return the underlying linked list
// Args: none
// Ret: m_list
// Complexity: 0(n)
//========================================================================
DLinkedList<Player> Database::getList()
{
	return m_list;
}
