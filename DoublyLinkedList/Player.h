#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <iostream>

class Player
{
private:
	std::string m_first;
	std::string m_last;
	int m_lvl;
	int m_exp;
	//int m_playerNo;
public:
	Player();
	Player(std::string first, std::string last, int xp, int lvl);
	int getLvl();
	int getExp();
	std::string getFirst();
	std::string getLast();
	void setFirst(std::string first);
	void setLast(std::string last);
	void setLvl(int lvl);
	void setExp(int exp);
	//std::ostream& operaor<<(std::ostream &strm, Player& p);
	void print();
	friend std::ostream& operator<<(std::ostream& os, Player& dt);
	bool equals(Player& p);
};
#endif