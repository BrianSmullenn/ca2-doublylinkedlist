#ifndef DOUBLYLINKEDLIST_H
#define DOUBLYLINKEDLIST_H
#include "Player.h"

// This is going to be our custom linked list template class, it includes:
// DListNode to model a node in the list
// DLinkedList to model the list
// DListIterator to iterate through the list

// forward class declarations - tell compiler there will be stuff later, calm yo ass down!
template<class DataType> class DListNode;
template<class DataType> class DLinkedList;
template<class DataType> class DListIterator;


//========================================================================
// Name: DListNode
// Desc: Basic singky linked list node class
//========================================================================
template<class DataType>
class DListNode
{
public:
	//========================================================================
	// Name: m_data
	// Desc: data stored in each node
	//========================================================================
	DataType m_data;

	//========================================================================
	// Name: m_next
	// Description: a pointer to the next node in the list
	//========================================================================
	DListNode<DataType>* m_next;

	//========================================================================
	// Name: m_previous
	// Desc: A pointer to previous node in the list
	//========================================================================
	DListNode<DataType>* m_previous;

	//========================================================================
	// Name: DListNode
	// Desc: a constructor which constructs an empty node
	//========================================================================
	DListNode() : m_next(0),m_previous(0){}

	//========================================================================
	// Name: insertAfter
	// Desc: insert a new node after current node
	// Args: p_data - the data to insert
	// Complexity: Only ever works on one element so never expands. 
	// See insert for use.
	//========================================================================
	void insertAfter(DataType p_data)
	{
		// create a new node
		DListNode<DataType>* newNode = new DListNode<DataType>;
		newNode->m_data = p_data;
		
		//// make new node point to next node and previous
		//newNode->m_next = this->m_next;
		//m_next->m_previous = newNode;
		////this->m_next = m_previous;
		//newNode->m_previous = this;
		//// make the previous node point at newnode and next node at new node too.
		//this->m_next = newNode;

		//New nodes pointing to next and previous.
		newNode->m_next = this->m_next;
		newNode->m_previous = this;

		//this->m_next->m_previous = newNode;
		this->m_next = newNode;
	}
};

//========================================================================
// Name: DListIterator
// Desc: the iterator
//========================================================================
template<class DataType>
class DListIterator
{
public:
	DLinkedList<DataType>* m_list;
	DListNode<DataType>* m_node;

	//========================================================================
	// Name: DListIterator
	// Desc: constructor
	//========================================================================
	DListIterator(DLinkedList<DataType>* p_list = 0, DListNode<DataType>* p_node = 0)
	{
		m_list = p_list;
		m_node = p_node;
	}

	//========================================================================
	// Name: start
	// Desc: moves iterator to start of list
	//========================================================================
	void start()
	{
		if (m_list != 0)
		{
			m_node = m_list->m_head;
		}
	}

	//========================================================================
	// Name: start
	// Desc: moves iterator to end of list
	//========================================================================
	void tail()
	{
		if (m_list != 0)
		{
			m_node = m_list->m_tail;
		}
	}

	//========================================================================
	// Name: forth
	// Desc: moves iterator forward
	//========================================================================
	void forth()
	{
		if (m_list != 0)
		{
			m_node = m_node->m_next;
		}
	}

	//========================================================================
	// Name: BackWard
	// Desc: moves iterator backward
	//========================================================================
	void back()
	{
		if (m_list != 0)
		{
			m_node = m_node->m_previous;
		}
	}

	//========================================================================
	// Name: valid
	// Desc: checks if iterator is valid
	// Rtrn: bool - true if the iterator is valid
	//========================================================================
	bool valid()
	{
		return (m_node != 0);
	}

	//========================================================================
	// Name: item
	// Desc: returns item iterator points to
	// Rtrn: reference to current item
	//========================================================================
	DataType& item()
	{
		return m_node->m_data;
	}
};

//========================================================================
// Name: DLinkedList
// Desc: singly linked list container class
//========================================================================
template <class DataType>
class DLinkedList
{
public:
	DListNode<DataType>* m_head;
	DListNode<DataType>* m_tail;
	int m_count;
	//========================================================================
	// Name: DLinkedList
	// Desc: constructor - creates empty list
	//========================================================================
	DLinkedList() :m_head(0), m_tail(0), m_count(0){}

	//========================================================================
	// Name: ~DLinkedList
	// Desc: destructor - destroys linked list to free up memory
	//========================================================================
	~DLinkedList()
	{
		// temporary node pointer
		DListNode<DataType>* itr = m_head;
		DListNode<DataType>* next;
		while (itr != 0)
		{
			// save pointer to next node
			next = itr->m_next;
			// delete the current node
			delete itr;
			// make next node the current node
			itr = next;
		}
	}

	//========================================================================
	// Name: append
	// Desc: appends onto the end of the list the passed in data
	// Args: p_data - the data to append
	// Complexity: 0(n)
	// Timing:
	//========================================================================
	void append(DataType p_data)
	{

		// check if the list is empty
		if (m_head == 0)
		{
			// create a new head 
			m_head = m_tail = new DListNode<DataType>;
			m_head->m_data = p_data;
		}
		else
		{
			//Create a new node and set its data
			DListNode<DataType>* newNode = new DListNode<DataType>;
			newNode->m_data = p_data;
			//current tail's next node should be new node
			m_tail->m_next = newNode;
			//newNodes previous is the current tail
			newNode->m_previous = m_tail;
			//Tail is new node
			m_tail = newNode;
		}
		m_count++;
	}
	
	
	//========================================================================
	// Name: prepend
	// Desc: prepends onto the front of the list the passed in data
	// Args: p_data - the data to prepend
	//========================================================================
	void prepend(DataType p_data)
	{
		// create new node
		DListNode<DataType>* newNode = new DListNode<DataType>;
		newNode->m_data = p_data;
		newNode->m_next = m_head;
		m_head->m_previous = newNode;
		// set the head node, and the tail node if required
		m_head = newNode;
		if (m_tail == 0)
		{
			m_tail = m_head;
		}
		m_count++;
	}

	//========================================================================
	// Name: insert
	// Desc: inserts the passed in data into the list
	// at the location after the given iterator or at the end if itr not valid
	// Args: p_iterator - the place to insert the data past
	// Args: p_data - the data to insert
	// Complextity: 0(c) - constant order
	// Time: 100000 Integers = 107ms
	// Time: 10000000 Ints = 10112ms 
	// Time: Both 0ms per value
	//========================================================================
	void insert(DListIterator<DataType>& p_iterator, DataType p_data)
	{
		// if the iterator doesnt belong to this list, do nothing
		if (p_iterator.m_list != this)
		{
			//cout << "Break" << endl;
			return;
		}
		else
		{
			// make sure iterator is pointing to a non-null node
			if (p_iterator.m_node != 0)
			{
				// iterator is valid
				p_iterator.m_node->insertAfter(p_data);

				// if the iterator is the tail node then we need to update the tail node
				// COME BACK HERE!!!!!!!!
				if (p_iterator.m_node == m_tail)
				{
					m_tail = p_iterator.m_node->m_next;
				}
				
				//std::cout << "Should work" << sendl;
			}
			else
			{
				append(p_data);
			}
		}
	}

	//========================================================================
	// Name: insert (Overloaded)
	// Desc: inserts the passed in data at a passed in position
	// Args: p_position - the place to insert the data
	// Args: p_data - the data to insert
	// Complextity: 0(c) - constant order //COME BACK!!!!!!!!
	// Time: 100000 Integers = 107ms
	// Time: 10000000 Ints = 10112ms 
	// Time: Both 0ms per value
	//========================================================================
	void insert(int p_position, DataType p_data)
	{
		//Brief says position 0 is before the head so
		//I assume everything from 1 and below should 
		//be just prepended
		if(p_position <= 1)
		{
			prepend(p_data);
			m_count++;
			return;
		}
		else if(p_position >= m_count)
		{
			append(p_data);
			m_count++;
			return;
		}
		else 
		{
			DListIterator<DataType> itr = getIterator();
			int count = 0;
		
			for (itr.start(); itr.valid(); itr.forth())
			{
				count++;
				if(count == p_position - 1)
				{
					insert(itr, p_data);
					//itr.m_node->m_next = itr.m_node->m_next->m_node;
					itr.forth();
				
					//Set the next node's previous node to newNode.
					itr.m_node->m_next->m_previous = itr.m_node;
					m_count++;
					return;
				}
			}
		}
		
		//append(p_data);
	}

	//========================================================================
	// Name: remove
	// Desc: removes the node at the location
	// of the passed in iterator from the list
	// Args: p_iterator - the place to remove the data from
	//========================================================================
	void remove(DListIterator<DataType>& p_iterator)//Some sort of problem or some shit
	{
		// if the iterator doesnt belong to this list, do nothing
		if (p_iterator.m_list != this)
		{
			return;
		}
		if (p_iterator.m_node == 0)
		{
			return;
		}
		
		//DListNode<DataType>* node = m_head;
		
		if (p_iterator.m_node == m_head)
		{
			removeHead();
		}
		else if (p_iterator.m_node == m_tail)
		{
			removeTail();
		}
		else
		{
			//now im at the node i want to delete

			p_iterator.m_node->m_previous->m_next = p_iterator.m_node->m_next;
			p_iterator.m_node->m_next->m_previous = p_iterator.m_node->m_previous;

			// delete the node 
			delete p_iterator.m_node;
			p_iterator.m_node = 0;
			
		}
		m_count--;
	}

	//========================================================================
	// Name: Remove
	// Desc: Remove node at p_position
	// Args: p_position. Where to remove
	// Complexity: 0(n)
	//========================================================================
	void remove(int p_position)
	{
		//Brief says position 0 is before the head so
		//I assume everything from 1 and below should 
		//be just prepended
		if(p_position <= 1)
		{
			removeHead();
			return;
		}
		else if(p_position >= m_count)
		{
			removeTail();
			return;
		}
		else 
		{
			DListIterator<DataType> itr = getIterator();
			int count = 0;
		
			for (itr.start(); itr.valid(); itr.forth())
			{
				count++;
				if(count == p_position)
				{
					remove(itr);
					return;
				}
			}
		}
	}

	//========================================================================
	// Name: removeHead
	// Desc: removes the head of the list
	// Complextity: 0(c)
	//========================================================================
	void removeHead()
	{
		DListNode<DataType>* node = 0;
		// check list is not empty
		if (m_head != 0)
		{
			// make node be the next node
			node = m_head->m_next;
			// then delete the head, update head to point to node
			delete m_head;
			m_head = node;

			// if the head is null we have just deleted the only element in the list, so we need to update the tail
			if (m_head == 0)
			{
				m_tail = 0;
			}
			m_count--;
		}
	}

	//========================================================================
	// Name: removeTail
	// Desc: removes the tail of the list
	// Complexity: 0(c)
	//========================================================================
	void removeTail()
	{
		DListNode<DataType>* node = m_head;
		// check list is not empty
		if (m_head != 0)
		{
			// if head and tail are the same, the list has one node
			if (m_head == m_tail)
			{
				// delete the node
				delete m_head;
				// then set both to zero
				m_head = m_tail = 0;
			}
			else
			{
				// skip to second last node
				while (node->m_next != m_tail)
				{
					node = node->m_next;
				}
				// make tail point to current node aka node before tail
				m_tail = node;
				// delete tail
				delete node->m_next;
				node->m_next = 0;
			}
			m_count--;
		}
	}

	//========================================================================
	// Name: getIterator
	// Desc: gets an iterator pointing to the start of the current list
	// Rtrn: an iterator
	//========================================================================
	DListIterator<DataType> getIterator()
	{
		return DListIterator<DataType>(this, m_head);
	}

	//========================================================================
	// Name: Find
	// Desc: gets correct iterator for a piece of data
	// Args: p_data data to search for.
	// Complexity: 0(n)
	//========================================================================
	DListIterator<DataType> find(DataType p_data)
	{
		DListIterator<DataType> itr = this->getIterator();
		for (itr.tail(); itr.valid(); itr.back())
		{
			if(itr.item() == p_data)
			{
				return itr;
				cout << "Successful" << endl;
			}
		}
	}

	//========================================================================
	// Name: getTail
	// Desc: return the pointer to tail mode
	// Complexity: 0(c)
	//========================================================================
	DListNode<DataType>* getTail()
	{
		return m_tail;
	}

	//========================================================================
	// Name: size
	// Desc: get the no of elements in list. 
	// Complexity: 0(c)
	//========================================================================
	int size()
	{
		return m_count;
	}

	//========================================================================
	// Name: Contains
	// Desc: check if list contains an item
	// Args: p_data data to check
	// Ret: boolean. True if contains p_data
	// Complexity: 0(n)
	//========================================================================
	bool contains(DataType& p_data)
	{
		DListIterator<DataType> itr = m_list.getIterator();

		for (itr.start(); itr.valid(); itr.forth())
		{
			if(itr.item() == p_data)
			{
				return true;
			}
		}
		return false;
	}
};
#endif