// miao miao miao miao miao miao miao miao miao miao miao miao miao miao miao miao
//Implementation of timer - provides a millisecond timer, time and date stamp functions
#include "Timer.h"
#include <ctime>

#ifdef _WIN32
	#include <Windows.h>
#else
	#include <sys/time.h>
#endif

#ifdef _WIN32
	class Win32PerformanceCounter
	{
	//private:
	//	lint m_frequency;
	public:
		lint m_frequency;
		Win32PerformanceCounter()
		{
			// get the ticks per second
			QueryPerformanceFrequency((LARGE_INTEGER*) (&m_frequency));
			// convert to ticks per millisecond
			m_frequency  = m_frequency / 1000;
		}
	};

	Win32PerformanceCounter g_win32PerfCounter;
#endif

	//=============================================================================
	// These functions are going to get a time value.
	// The actual meaning of the time is only relative.
	//=============================================================================

	lint GetTimeMS()
	{
		#ifdef _WIN32
		lint t;
		QueryPerformanceCounter((LARGE_INTEGER*) (&t));
		return t/g_win32PerfCounter.m_frequency;
		#else
		struct timeval t;
		lint s;
		gettimeofday(&t, 0);
		// calculate number of milliseconds from second part
		s = t.tv_sec;
		s *= 1000;
		// calculate number of milliseconds from microsecond part
		s += (t.tv_usec / 1000);

		return s;
		#endif
	}

	lint GetTimeS()
	{
		return GetTimeMS() / 1000;
	}
	lint GetTimeM()
	{
		return GetTimeMS() / 60000;
	}
	lint GetTimeH()
	{
		return GetTimeMS() / 360000;
	}

	//lint getTimeD()
	//{
	//	return getTimeMS() / 1000;
	//}
	//lint getTimeY()
	//{
	//	return getTimeMS() / 1000;
	//}

	std::string TimeStamp()
	{
		char str[9];
		// get time and convert into struct tm format
		time_t a = time(0);
		struct tm b;
		localtime_s(&b, &a);
		// print time to a string 
		strftime(str, 9, "%H:%M:%S", &b);

		return str;

	}

	// prints datestamp in format YYYY:MM:DD
	std::string DateStamp()
	{
		char str[11];
		// get date and convert into struct tm format
		time_t a = time(0);
		struct tm b;
		localtime_s(&b, &a);
		// print date to a string 
		strftime(str, 11, "%Y:%m:%d", &b);

		return str;
	}

	
	// constructor with initializer list
	Timer::Timer():m_startTime(0), m_initTime(0) // initializer lists are faster
	{
	}

	void Timer::Reset(lint p_timepassed)
	{
		m_startTime = p_timepassed;
		m_initTime = GetTimeMS();
	}

	lint Timer::GetMS()
	{
		// return the amount of time that has passed since the timer was initialized + whatever starting time the timer had
		return (GetTimeMS() - m_initTime) + m_startTime;
	}

	lint Timer::GetTimeS()
	{
		return GetMS()/1000;
	}

	lint Timer::GetTimeM()
	{
		return GetMS()/(60*1000);
	}

	lint Timer::GetTimeH()
	{
		return GetMS()/(60*60*1000);
	}

	lint Timer::GetTimeD()
	{
		return GetMS()/(24*60*60*1000);
	}

	lint Timer::GetTimeY()
	{
		lint x = 60 * 60 * 1000 * 24; // shane made me do this, which is what i'd have done, noticed this problem in class
         x *= 365;

		return this->GetMS() / x;
	}

	std::string Timer::getString()
	{
		std::string str;
		lint y = GetTimeY();
		lint d = GetTimeD()%365; // integer division from above leaves no remainder(intrithmatic) so % needed here
		lint h = GetTimeH()%24;
		lint m = GetTimeM()%60;

		if(y > 0)
		{
			str += y + " years, ";
		}
		if(d > 0)
		{
			str += d + " days, ";
		}
		if(h > 0)
		{
			str+= h + " hours, ";
		}
		if(m > 0)
		{
			str += m + " minutes";
		}

		return str;
	}