#ifndef TIMER_H
#define TIMER_H
//This class is a timer class which provides a millisecond timer, with other timers based on it.
//Millisecond timer implemented using queryPerformanceCounter/Frequency.
#include <string>

typedef long long int lint;
lint getTimeMS();
lint getTimeS();
lint getTimeM();
lint getTimeH();

std::string timeStamp();
std::string dateStamp();

//=============================================================================
//The Timer Class
//=============================================================================
class Timer
{
private:
	// this is the time at which the timer is initialized
	lint m_initTime;
	// this is the official start time of the timer
	lint m_startTime;

public:
	Timer();
	void Reset(lint p_timePassed = 0); // if the user doesnt pass in an arguement, it is passed in as zero
	lint GetMS();
	lint GetTimeS();
	lint GetTimeM();
	lint GetTimeH();
	lint GetTimeD();
	lint GetTimeY();

	std::string getString();
};

inline lint seconds(lint t){return t*1000;}
inline lint minutes(lint t){return t*1000*60;}
inline lint hours(lint t){return t*1000*60*60;}
inline lint days(lint t){return t*1000*60*60*24;}
inline lint years(lint t){return t*1000*60*60*24*365;}


#endif // !TIMER_H
