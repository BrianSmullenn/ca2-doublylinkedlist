#include "Player.h"
#include <iostream>
using std::cout;
using std::endl;

//========================================================================
// Name: Player
// Desc: default Constructor
//========================================================================
Player::Player()
{
	m_first ="";
	m_last ="";
	m_exp = 0;
	m_lvl = 0;
}

//========================================================================
// Name: player
// Desc: Constructor
// Args: string first, string last, int xp, int lvl - set as member vars
//========================================================================
Player::Player(std::string first, std::string last, int xp, int lvl)
{
	m_first = first;
	m_last = last;

	m_exp = xp;
	m_lvl = lvl;
	
}

//========================================================================
// Name: GetExp
// Desc: return m_exp
// ret: m_exp
//========================================================================
int Player::getExp()
{
	return m_exp;
}

//========================================================================
// Name: GetFirst
// Desc: return m_first
// ret: m_first
//========================================================================
std::string Player::getFirst()
{
	return m_first;
}

//========================================================================
// Name: GetLast
// Desc: return m_last
// ret: m_last
//========================================================================
std::string Player::getLast()
{
	return m_last;
}

//========================================================================
// Name: GetLvl
// Desc: return m_lvl
// ret: m_lvl
//========================================================================
int Player::getLvl()
{
	return m_lvl;
}

//========================================================================
// Name: SetExp
// Desc: set m_exp to passed in value
// Args: xp - set m_exp to this
//========================================================================
void Player::setExp(int xp)
{
	m_exp = xp;
}

//========================================================================
// Name: SetLvl
// Desc: set m_lvl to passed in value
// Args: lvl - set m_lvl to this
//========================================================================
void Player::setLvl(int lvl)
{
	m_lvl = lvl;
}

//========================================================================
// Name: SetFirst
// Desc: set m_first to passed in value
// Args: first - set m_first to this
//========================================================================
void Player::setFirst(std::string first)
{
	m_first = first;
}

//========================================================================
// Name: SetLast
// Desc: set m_last to passed in value
// Args: last - set m_last to this
//========================================================================
void Player::setLast(std::string last)
{
	m_last = last;
}

//========================================================================
// Name: Print
// Desc: Prints out the player object
//========================================================================
void Player::print()
{
	cout << endl;
	cout << "\n FirstName:\t" << m_first
		 << "\n LastName:\t" << m_last
		 << "\n LV:\t\t" << m_lvl
		 << "\n XP:\t\t" << m_exp << endl;
}

//========================================================================
// Name: operator <<
// Desc: overide operator << to print player object
// Args: os - ostream
//       p - player
// ret: ostream to print
//========================================================================
std::ostream& operator<<(std::ostream& os, Player& p)
{
	os << "\n FirstName:\t" << p.getFirst()
		<< "\n LastName:\t" << p.getLast()
		<< "\n LV:\t\t" << p.getLvl()
		<< "\n XP:\t\t" << p.getExp() << endl;
    return os;
}

//========================================================================
// Name: Equals
// Desc: check if one player is equal to another player
// Args: p - player to check is equals to "this"
// Return: bool - true if equal
//========================================================================
bool Player::equals(Player& p)
{
	if(this->m_first != p.getFirst())
	{
		return false;
	}
	if(this->m_last != p.getLast())
	{
		return false;
	}
	/*if(this->m_lvl != p.m_lvl)
	{
		return false;
	}
	if(this->m_exp != p.m_exp)
	{
		return false;
	}*/
	return true;

}