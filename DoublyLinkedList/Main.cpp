//This is the driver application for my doubly linked list
#include "DoublyLinkedList.h"
#include <iostream>
#include "Timer.h"
#include "Database.h"
#include "Player.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;

template <class datatype>
void printList(DLinkedList<datatype>& p_list);

void printMenu();
int getInput();
string getStringInput();
Player createPlayer();
void printPlayerList(DLinkedList<Player>& p_list);

// MSVC memory-leak-checking code
#ifdef _MSC_VER
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

int main()
{
	// MSVC memory-leak-checking code
    #ifdef _MSC_VER
		#if defined(DEBUG) | defined(_DEBUG)
            _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
        #endif
    #endif

	Database dbase;
	
	
	DLinkedList<Player> empty;
	enum options {INSERT = 1, REMOVE = 2, SEARCH = 3, AVERAGE = 4,
		DUPLICATES = 5, PRINT = 6,EXIT = 7, TEST = 8};
	
	bool go = true;
	int input = 0;
	int input2 = 0;
	string stringInput = "";
	string stringInput2 = "";
	int min = 0;
	int max = 0;
	Timer t;
	lint after;

	//Create some players //Test Data
	Player p1("Brian", "Smullen", 15000, 15);
	Player p2("Kevin", "Duffy", 3000, 3);
	Player p3("Mark", "Barry", 5500, 5);
	Player p4("Matthew", "Walsh", 1000, 1);
	Player p5("Shane", "Gavin", 100, 1);
	Player p6("Lauren","O'Reilly", 12000, 12);
	Player p7("Lorrin", "Cunningham", 1, 0);
	Player p8("Lauren","O'Reilly", 10000, 10);
	Player p9("Flea", "from RHCP", 4, 4600);
	Player p10("Barry", "White", 10000, 10);
	Player p11("Bilbo", "Baggins", 19300, 19);
	Player p12("Barney", "The Dinosaur", 2900, 2);
	Player p13("Scott", "Cronin", 200, 1);
	Player p14("The King", "Rennicks", 300, 1);
	Player p15("A guy named", "Paul", 5000, 5);
	Player p16("Sean", "of the Clarke clan", 8700, 8);
	Player p17("Mister","Nintendo", 100000, 100);
	Player p18("Pika", "Chu", 5000000, 5000);
	Player p19("Derpa", "Derp", 1, 1);
	Player p20("Derp", "Derpa", 1, 1);
	Player p21("Derpina", "Derpovichski", 1, 1);
	Player p22("Finula", "Fluff", 4400, 4);
	Player p23("Frank", "Just Frank", 20000, 20);
	Player p24("Lauren", "O'Reilly", 4400, 4);
	Player p25("Shaq", "", 100500, 100);
	Player p26("Vladamir","Putin", 1000, 1);
	Player p27("Genghis", "Khan", 5000000, 5000);
	Player p28("Tinky", "Winky", 100, 1);
	Player p29("Gandalf", "The Grey", 4000, 4);
	Player p30("Gandalf", "The White", 99999999, 9999);

	for(int i = 0; i != 1000; i++)
	{
	dbase.append(p1);
	}
	Player temp;
	while(go!=false)
	{
		DLinkedList<Player> searchResults;
		Database noDupes;
		printMenu();
		input = getInput();
		switch (input)
		{
			case INSERT:
			{
				temp = createPlayer();
				cout << "put where?" << endl;
				input = getInput();
				t.Reset();
				dbase.insert(input, temp);
				after = t.GetMS();
				cout << "Insert took " << after << " ms." << endl;
				break;
			}

			case REMOVE:
			{
				cout << "Enter the position to remove from." << endl;
				input = getInput();
				t.Reset();
				dbase.remove(input);
				after = t.GetMS();
				cout << "Remove took " << after << " ms." << endl;
				break;
			}
			case SEARCH:
			{
				enum searchkeys{LEVEL = 0, EXP = 1, FIRST = 3, SECOND = 4};
				cout << "What would you like to search by? 0)Level 1)Experience 2)Firstname 3)lastname" << endl;
				input = getInput();
				if (input == LEVEL || input == EXP) // possibly put into methods? code duplication at the get average part below
				{
					cout << "Would you like to search within a range? 1)Yes 2)No" << endl;
					input2 = getInput();
					if (input2 == 1)
					{
						cout << "Enter min" << endl;
						min = getInput();
						cout << "Enter max" << endl;
						max = getInput();
						t.Reset();
						dbase.search(input, min, max, searchResults);
						after = t.GetMS();
					}
					else
					{
						cout << "Enter value to search for" << endl;
						input2 = getInput();
						t.Reset();
						dbase.search(input, input2, searchResults);
						after = t.GetMS();
					}
				}
				else
				{
					cout << "Enter value to search for" << endl;
					stringInput = getStringInput();
					t.Reset();
					dbase.search(input, stringInput, searchResults);
					after = t.GetMS();
				}
				printPlayerList(searchResults);
				cout << "Search took " << after << " ms." << endl;
				break;
			}
			case AVERAGE:
			{
				cout << "Enter the level you wish the view the average experience for" << endl;
				input = getInput();
				int avg;
				t.Reset();
				avg = dbase.getAvgExperience(input);
				after = t.GetMS();
				cout << "Average experience: " << avg << " took " << after << " ms." << endl;
				break;
			}
			case DUPLICATES:
			{
				int dupes;
				t.Reset();
				dupes = dbase.removeDuplicates();
				after = t.GetMS();
				cout << dupes << " duplicates removed in " << after << " ms." << endl;
				//dbase = removeDups;
				//cout << "Would you like to print the modified list. 1)Yes 2)No" << endl;
				//dbase.print();
				break;
			}
			case PRINT:
			{
				dbase.print();
				break;
			}
			case EXIT:
			{
				go = false;
				break;
			}
		}
	}
	return 0;
}

//Simply prints a doubly Linked list
template<class Datatype>
void printList(DLinkedList<Datatype>& p_list)
{
	DListIterator<Datatype> itr = p_list.getIterator();
	cout << "list contains: ";
	for (itr.start(); itr.valid(); itr.forth())
	{
		cout << itr.item() << ", ";
	}

	cout << "\b\b " << endl;
}

//prints a list of players
void printPlayerList(DLinkedList<Player>& p_list)
{
	DListIterator<Player> itr = p_list.getIterator();
	for (itr.start(); itr.valid(); itr.forth())
	{
		itr.item().print();
	}
}

//print the main menu
void printMenu()
{
	cout << "=========================================================" << endl;
	cout << "1) Insert a new record at a particular position"<< endl;
	cout << "2) Delete a record from a particular position" << endl;
	cout << "3) Search database and print results" << endl;
	cout << "4) Average Exp at a particular level" << endl;
	cout << "5) Find and remove all duplicate entries" << endl;
	cout << "6) Print Database" << endl;
	cout << "7) Exit" << endl;
	cout << "=========================================================" << endl;
}

// get input from player. Got the validation code off of michael campbell
int getInput()
{
	cout << endl;
	cout << ">";
	int input;
	
	while(!(cin >> input))
	{
		cout << "Invalid, try again:" << endl;
		cin.clear();
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	}

	return input;
}

// get string input same while as above
string getStringInput()
{
	cout << endl;
	cout << ">";
	string input;
	
	while(!(cin >> input))
	{
		cout << "Invalid, try again:" << endl;
		cin.clear();
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	}

	return input;
}

// prompt user to create and return player
Player createPlayer()
{
	Player p;
	cout << "Player first name: " << endl;
	p.setFirst(getStringInput());
	cout << "Player second name: " << endl;
	p.setLast(getStringInput());
	cout << "Player level: " << endl;
	p.setLvl(getInput());
	cout << "Player experience: " << endl;
	p.setExp(getInput());
	return p;
}