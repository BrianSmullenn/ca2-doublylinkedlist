//This is the data base class. It will contain a linked list and also demonstrate all of the lists uses.
#ifndef DATABASE_H
#define DATABASE_H
#include "DoublyLinkedList.h"
#include "Player.h"
class Database
{
private:
	DLinkedList<Player> m_list;
	DListIterator<Player> m_itr;
public:
	void append(Player p_Player);
	void insert(int p_position, Player p_player);
	void remove(int p_position);
	void search(int p_choice, int p_data, DLinkedList<Player>& p_results);
	void search(int p_choice, int p_min, int p_max, DLinkedList<Player>& p_results);
	void search(int p_choice, std::string p_data, DLinkedList<Player>& p_results);
	int getAvgExperience(int lvl);
	int removeDuplicates();
	bool contains(Player& p_p);
	void print();
	DLinkedList<Player> getList();
};

#endif